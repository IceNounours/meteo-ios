//
//  Villes.swift
//  meteo-ios
//
//  Created by Thomas Fromont on 25/06/2017.
//  Copyright © 2017 Thomas Fromont. All rights reserved.
//

import Foundation

public enum Ville {
    case tokyo
    case newyork
    case riodejaneiro
    case london
    case capetown
    
    var name: String {
        get {
            switch self {
            case .tokyo:
                return "Tokyo"
            case .newyork:
                return "New York"
            case .riodejaneiro:
                return "Rio de Janeiro"
            case .london:
                return "Londres"
            case .capetown:
                return "Le Cap"
            }
        }
    }
    
    var lattitude: Double {
        get {
            switch self {
            case .tokyo:
                return 35.6895
            case .newyork:
                return 40.7128
            case .riodejaneiro:
                return -22.9068
            case .london:
                return 51.5074
            case .capetown:
                return -33.9249
            }
        }
    }
    
    var longitude: Double {
        get {
            switch self {
            case .tokyo:
                return 139.6917
            case .newyork:
                return -74.0059
            case .riodejaneiro:
                return -43.1729
            case .london:
                return -0.1278
            case .capetown:
                return 18.4241
            }
        }
    }
}
