//
//  VilleVC.swift
//  meteo-ios
//
//  Created by Thomas Fromont on 25/06/2017.
//  Copyright © 2017 Thomas Fromont. All rights reserved.
//

import UIKit

class VilleVC: UIViewController {

    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var lbTemperature: UILabel!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lbPressure: UILabel!
    @IBOutlet weak var lbHumidity: UILabel!
    @IBOutlet weak var lbSummary: UILabel!
    @IBOutlet weak var lbPrecipitationIntensity: UILabel!
    @IBOutlet weak var lbPrecipitationAccumulation: UILabel!
    @IBOutlet weak var lbWindSpeed: UILabel!
    @IBOutlet weak var lbWindGust: UILabel!
    
    var ville: VilleModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initView()
    }
    
    func initView() {
        if let v = ville {
            imgIcon.image = v.icon
            lbName.text = v.name
            
            if let temperature = v.temperature {
                lbTemperature.text = "Temperature : \(temperature)"
            } else {
                lbTemperature.text = ""
            }
            
            if let pressure = v.pressure {
                lbPressure.text = "Pression : \(pressure)"
            } else {
                lbPressure.text = ""
            }
            
            if let humidity = v.humidity {
                lbHumidity.text = "Humidité : \(humidity)"
            } else {
                lbHumidity.text = ""
            }
            
            if let summary = v.summary {
                lbSummary.text = "Ciel : \(summary)"
            } else {
                lbSummary.text = ""
            }
            
            if let precipitationIntensity = v.precipitationIntensity {
                lbPrecipitationIntensity.text = "Intensité des précipitacions : \(precipitationIntensity)"
            } else {
                lbPrecipitationIntensity.text = ""
            }
            
            if let windSpeed = v.windSpeed {
                lbWindSpeed.text = "Vitesse du vent : \(windSpeed)"
            } else {
                lbWindSpeed.text = ""
            }
            
            if let windGust = v.windGust {
                lbWindGust.text = "Fluctuation du vent : \(windGust)"
            } else {
                lbWindGust.text = ""
            }
        }
    }
}

