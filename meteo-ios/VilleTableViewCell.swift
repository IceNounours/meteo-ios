//
//  VilleTableViewCell.swift
//  meteo-ios
//
//  Created by Thomas Fromont on 25/06/2017.
//  Copyright © 2017 Thomas Fromont. All rights reserved.
//

import UIKit


class VilleTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var lbTemperature: UILabel!
    
    open func update(ville: VilleModel) {
        // Icon
        imgIcon.image = ville.icon
        
        // Name
        lbName.text = ville.name
        
        // Name
        lbTemperature.text = ville.temperature
    }
}
