//
//  UIViewController+Alert.swift
//  meteo-ios
//
//  Created by Thomas Fromont on 25/06/2017.
//  Copyright © 2017 Thomas Fromont. All rights reserved.
//

import UIKit
import PopupDialog

extension UIViewController {
    
    /**
     Affiche une popup avec un message et un bouton Fermer.
     - parameter message: message à afficher, par défaut rien
     - parameter completion: l'action à exécuter quand la popup se ferme, par défaut rien
     */
    func showAlert(_ message: String? = nil, completion: (()->())? = nil) {
        let message = message ?? ""
        // Create the dialog
        let popup = PopupDialog(title: "", message: message)
        // Create buttons
        let btnFermer = CancelButton(title: NSLocalizedString("fermer", comment: "")) {
            completion?()
        }
        popup.addButton(btnFermer)
        
        // Present dialog
        self.present(popup, animated: true, completion: nil)
    }
}
