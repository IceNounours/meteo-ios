//
//  MeteoVC.swift
//  meteo-ios
//
//  Created by Thomas Fromont on 25/06/2017.
//  Copyright © 2017 Thomas Fromont. All rights reserved.
//

import UIKit
import SwiftSky

class MeteoVC: UITableViewController {

    var detailViewController: VilleVC? = nil
    var villeItems = [VilleModel]()
    
    var villes = [Ville.tokyo, Ville.newyork, Ville.riodejaneiro, Ville.london, Ville.capetown]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        initSky()
        for ville in villes {
            callSky(ville)
        }
    }
    
    func initSky() {
        SwiftSky.secret = "7ace40e289be24355e7a191ebabe482e"
        
        SwiftSky.hourAmount = .fortyEight
        SwiftSky.language = .french
        SwiftSky.locale = .autoupdatingCurrent
        
        SwiftSky.units.temperature = .celsius
        SwiftSky.units.distance = .kilometer
        SwiftSky.units.speed = .meterPerSecond
        SwiftSky.units.pressure = .millibar
        SwiftSky.units.precipitation = .millimeter
        SwiftSky.units.accumulation = .centimeter
    }
    
    func callSky(_ ville: Ville) {
        
        SwiftSky.get([.current], at: Location(latitude: ville.lattitude, longitude: ville.longitude)) {
            result in
            switch result {
            case .success(let forecast):
                let villeModel = VilleModel(ville.name, ville.lattitude, ville.longitude, forecast)
                self.villeItems.append(villeModel)
                self.tableView.reloadData()
            case .failure(_):
                self.showAlert(NSLocalizedString("error_api", comment: ""))
            }
        }
    }

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showVille" {
            if let indexPath = tableView.indexPathForSelectedRow {
                let ville = villeItems[indexPath.row]
                let controller = segue.destination as! VilleVC
                controller.ville = ville
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
        }
    }

    // MARK: - Table View

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return villeItems.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VilleCell", for: indexPath) as! VilleTableViewCell
        let ville = villeItems[indexPath.row]
        cell.update(ville: ville)
        return cell
    }
}

