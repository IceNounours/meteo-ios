//
//  VilleModel.swift
//  meteo-ios
//
//  Created by Thomas Fromont on 25/06/2017.
//  Copyright © 2017 Thomas Fromont. All rights reserved.
//

import Foundation
import SwiftSky

class VilleModel {
    
    public init(_ name: String, _ lattitude: Double, _ longitude: Double, _ forecast: Forecast) {
        self.name = name
        self.lattitude = lattitude
        self.longitude = longitude
        
        let icon = forecast.current?.icon ?? ""
        switch icon {
        case "clear-day":
            self.icon = #imageLiteral(resourceName: "clear-day")
        case "clear-night":
            self.icon = #imageLiteral(resourceName: "clear-night")
        case "partly-cloudy-day":
            self.icon = #imageLiteral(resourceName: "partly-cloudy-day")
        case "partly-cloudy-night":
            self.icon = #imageLiteral(resourceName: "partly-cloudy-night")
        case "cloudy":
            self.icon = #imageLiteral(resourceName: "cloudy")
        case "rain":
            self.icon = #imageLiteral(resourceName: "rain")
        case "sleet":
            self.icon = #imageLiteral(resourceName: "sleet")
        case "snow":
            self.icon = #imageLiteral(resourceName: "snow")
        case "wind":
            self.icon = #imageLiteral(resourceName: "wind")
        case "fog":
            self.icon = #imageLiteral(resourceName: "fog")
        default:
            self.icon = #imageLiteral(resourceName: "cloudy")
        }
        
        self.temperature = forecast.current?.temperature?.current?.label
        self.pressure = forecast.current?.pressure?.label
        self.humidity = forecast.current?.humidity?.label
        self.summary = forecast.current?.summary
        self.precipitationIntensity = forecast.current?.precipitation?.intensity?.label
        self.windSpeed = forecast.current?.wind?.speed?.label
        self.windGust = forecast.current?.wind?.gust?.label
    }
    
    var name: String
    var lattitude: Double
    var longitude: Double
    var icon: UIImage
    var temperature: String?
    var pressure: String?
    var humidity: String?
    var summary: String?
    var precipitationIntensity: String?
    var windSpeed: String?
    var windGust: String?
}
